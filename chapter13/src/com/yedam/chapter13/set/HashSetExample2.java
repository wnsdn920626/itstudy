package com.yedam.chapter13.set;

import java.util.HashSet;
import java.util.Set;

public class HashSetExample2 {

	public static void main(String[] args) {
		Set<Member> set = new HashSet<>();
		//같은 객체 중복 x
		
		set.add(new Member("홍길동", 30)); //100번지
		set.add(new Member("홍길동", 30)); //200번지
		
		System.out.println("총 객체 수 : " + set.size());
	}
	
}
