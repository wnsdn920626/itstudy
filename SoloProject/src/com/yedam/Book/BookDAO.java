package com.yedam.Book;

import java.util.List;



public interface BookDAO {

	// 전체조회
	
	List<BookVO> selectAll();
	
	//단건조회,
	
	List<BookVO> selectOne(BookVO bookVO);
	
	
	//도서 등록
	void insert(BookVO bookVO);
	
	//도서 삭제
	
	void delete(int bno);
	
	//도서 수정
	void update(BookVO bookVO);
	

	
	
}