package com.yedam.Book;

public class BookVO {
	
	public int bno;
	public String ISBN;
	public String cate;
	public String title;
	public String author;
	
	public String bordate;
	public String borwhether;
	public String loandate;
	public String tel;
	
	
	
	
	public int getBno() {
		return bno;
	}
	public void setBno(int bno) {
		this.bno = bno;
	}
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String ISBN) {
		this.ISBN = ISBN;
	}
	public String getCate() {
		return cate;
	}
	public void setCate(String cate) {
		this.cate = cate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getBordate() {
		return bordate;
	}
	public void setBordate(String bordate) {
		this.bordate = bordate;
	}
	public String getBorwhether() {
		return borwhether;
	}
	public void setBorwhether(String borwhether) {
		this.borwhether = borwhether;
	}
	public String getLoandate() {
		return loandate;
	}
	public void setLoandate(String loandate) {
		this.loandate = loandate;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	
	
	
	
}
