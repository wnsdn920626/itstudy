package com.yedam.Book;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.yedam.common.DAO;

public class BookDAOImpl extends DAO implements BookDAO{
	
	public static BookDAO instance = null;
	public static BookDAO getInstance() {
		if(instance == null)
			instance = new BookDAOImpl();
		return instance;
	}
	
	//전체조회
	@Override
	public List<BookVO> selectAll() {
		List<BookVO> list = new ArrayList<>();
		try {
			
			connect();
			
			stmt = conn.createStatement();
			String sql = "SELECT * FROM bookInfo";
			rs = stmt.executeQuery(sql); // stmt 정적	으로 쿼리 조회
			
			while(rs.next()) { //next() 마지막 열에 도달하면, false 값
				BookVO bookVO = new BookVO();
				bookVO.setBno(rs.getInt("bno"));
				bookVO.setISBN(rs.getString("ISBN"));
				bookVO.setCate(rs.getString("cate"));
				bookVO.setTitle(rs.getString("title"));
				bookVO.setAuthor(rs.getString("author"));
				
				list.add(bookVO);
			}
			
		}catch(Exception e){ // try구문에서 오류가 났을경우 catch로
			System.out.print("오류입니다.");
		}finally {
			disconnect();
		}
		return list;
	}
	
	//단건조회 -> 미구현, like OR사용하면 '%?%'이 적용안되고 테이블 전체출력이됨
	@Override
	public List<BookVO> selectOne(BookVO bookVO) {
		BookVO findVO = null;
		List<BookVO> list = new ArrayList<>();
		
		try {
			connect();
			
			String sql = "SELECT * "
					+ "FROM bookInfo "
					+ "WHERE (ISBN LIKE '%'||?||'%' "
					+ "AND cate LIKE '%'||?||'%'"
					+ "AND title LIKE '%'||?||'%'"
					+ "AND author LIKE '%'||?||'%')";
			
						
			
			
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, bookVO.getISBN());
			pstmt.setString(2, bookVO.getCate());
			pstmt.setString(3, bookVO.getTitle());
			pstmt.setString(4, bookVO.getAuthor());
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				findVO = new BookVO();
				
				findVO.setISBN(rs.getString("ISBN"));
				findVO.setCate(rs.getString("cate"));
				findVO.setTitle(rs.getString("title"));
				findVO.setAuthor(rs.getString("author"));
				
				list.add(findVO);
			}

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			disconnect();
		}
		return list;
	}

	// 도서등록 -> ISBN , 카테고리, 제목, 저자 순으로 입력하고 대여여부는 0(대여가능)으로 등록
	@Override
	public void insert(BookVO bookVO) {
		try {
			connect();
			String sql = "INSERT INTO bookInfo (ISBN,cate,title,author,borwhether) VALUES ( ?, ?, ?, ?, 0)";
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, bookVO.getISBN());
			pstmt.setString(2, bookVO.getCate());
			pstmt.setString(3, bookVO.getTitle());
			pstmt.setString(4, bookVO.getAuthor());
			
			
			int result = pstmt.executeUpdate();
			
			if(result >0) {
				
				System.out.println("도서가 정상적으로 등록되었습니다.");
			}else {
				System.out.println("도서가 정상적으로 등록되지않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		
	}
	
	//도서삭제 -> 도서번호로 도서 삭제가능
	@Override
	public void delete(int bno) {
		try {
			connect();
			stmt = conn.createStatement();
			String sql = "DELETE FROM bookInfo WHERE bno = " + bno;
			int result = stmt.executeUpdate(sql);
			
			if(result > 0) {
				
				System.out.println("정상적으로 삭제되었습니다.");
			}else {
				System.out.println("도서가 삭제되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		
	}
	
	//도서 수정 -> 카테고리 , 제목 , 저자 , 책번호순으로 수정가능
	@Override
	public void update(BookVO bookVO) {
		try {
			connect();
			String sql = "UPDATE bookInfo SET cate = ? , title = ? , author = ? where bno = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, bookVO.getCate());
			pstmt.setString(2, bookVO.getTitle());
			pstmt.setString(3, bookVO.getAuthor());
			pstmt.setInt(4, bookVO.getBno());

			int result = pstmt.executeUpdate();
			if(result > 0) {
				System.out.println("정상적으로 수정되었습니다.");
			}else {
				System.out.println("수정되지않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		
	
	}		
				
}
			
			
			
	
			
		
	
		
		
		
	

