package com.yedam.app;

import java.util.List;
import java.util.Scanner;

import com.yedam.Book.BookDAO;
import com.yedam.Book.BookDAOImpl;
import com.yedam.Book.BookVO;
import com.yedam.Borrow.BorrowDAOImpl;
import com.yedam.Borrow.BorrowDAO;
import com.yedam.Borrow.BorrowVO;
import com.yedam.loan.LoanVO;
import com.yedam.loan.LoanDAO;
import com.yedam.loan.LoanDAOImpl;



public class BookManagement {

	Scanner sc = new Scanner(System.in);

	BookDAO bookDAO = BookDAOImpl.getInstance();
	BorrowDAO borrowDAO = BorrowDAOImpl.getInstance();
	LoanDAO loanDAO = LoanDAOImpl.getInstance();
	public BookManagement() {
		while (true) {

			// 메뉴 출력
			menuPrint();

			// 메뉴 선택
			int menuNo = menuSelect();

			if (menuNo == 1) {
				SelectAll(); // 전체조회
			} else if (menuNo == 2) {
				selectOne();
				// select1();
			} else if (menuNo == 3) {
				insertBook();
			} else if (menuNo == 4) {
				deleteBno();
			} else if (menuNo == 5) {
				updateBno();
			} else if (menuNo == 6) {
				borSelect();
			} else if (menuNo == 7) {
				borUpdate();
			} else if (menuNo == 8) {
				loanUpdate();
			} else if (menuNo == 9) {
				loanSelect();
			} else if (menuNo == 0) {
				exit();
				break;
			} else {
				System.out.println("잘못입력하셨습니다.");
				System.out.println("메뉴 숫자를 입력해주세요");
			}
		}
	}

	

	// 메뉴 선택
	public int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("메뉴는 숫자로 구성되어있습니다.");
		}
		return menuNo;
	}

	// 메뉴 출력
	public void menuPrint() {
		System.out.println(
				"===============================================================================================================================");
		System.out.println("1.전체조회 2.단건조회 3.도서등록 4.도서삭제 5.도서수정 6.대출가능조회 7.도서대출 8.도서반납 9.미반납도서조회 0.프로그램종료");
		System.out.println(
				"===============================================================================================================================");
	}

	// 전체 조회
	public void SelectAll() {
		List<BookVO> list = bookDAO.selectAll();

		if (list.isEmpty()) {
			System.out.println("정보가 존재하지 않습니다.");
			return;
		}
		for (BookVO bookVO : list) {
			System.out.printf("%d. %s, %s, %s, %s \n", bookVO.getBno(), bookVO.getISBN(), bookVO.getCate(),
					bookVO.getTitle(), bookVO.getAuthor());
		}

	}

	public void selectOne() {
		BookVO findBook = inputBookInfo();
		List<BookVO> list = bookDAO.selectOne(findBook);

		if (list.isEmpty()) {
			System.out.print("해당 도서는 존재하지 않습니다.");

			return;
		}
		for (BookVO bookVO : list) {
			System.out.printf("%d. %s, %s, %s, %s \n", bookVO.getBno(), bookVO.getISBN(), bookVO.getCate(),
					bookVO.getTitle(), bookVO.getAuthor());
		}

	}
	public BookVO inputBookInfo() {

		BookVO bookVO = new BookVO();
		System.out.print("ISBN > ");
		bookVO.setISBN(sc.nextLine());
		System.out.print("정보(카테고리) > ");
		bookVO.setCate(sc.nextLine());
		System.out.print("제목 > ");
		bookVO.setTitle(sc.nextLine());
		System.out.print("저자 > ");
		bookVO.setAuthor(sc.nextLine());

		return bookVO;
	}



	public void insertBook() {
		BookVO bookVO = inputBookInfo();
		bookDAO.insert(bookVO);
	}

	public BookVO inputBookCode() {
		BookVO bookVO = new BookVO();

		System.out.println("정보(카테고리) > ");
		bookVO.setCate(sc.nextLine());
		System.out.println("제목 > ");
		bookVO.setTitle(sc.nextLine());
		System.out.println("저자 > ");
		bookVO.setAuthor(sc.nextLine());
		System.out.println("책번호 > ");
		bookVO.setBno(Integer.parseInt(sc.nextLine()));

		return bookVO;
	}

	public void deleteBno() {
		System.out.println("삭제할 도서번호를 입력하세요.");
		int bno = inputBno();
		bookDAO.delete(bno);
	}

	public int inputBno() {
		int bno = 0;
		try {
			bno = Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			System.out.println("도서 넘버로 입력해주세요");
		}
		return bno;
	}

	public void updateBno() {
		BookVO bookVO = inputBookCode();
		bookDAO.update(bookVO);
	}

	public void borSelect() {
			BorrowVO borrBook = inputborInfo();
			List<BorrowVO> list = borrowDAO.borSelect(borrBook);
			
			if(list.isEmpty()) {
				System.out.println();
				System.out.println("도서재고가 없습니다.");
				return;
			}
			for(BorrowVO borrowVO : list) {
				System.out.printf("%s , %s \n", borrowVO.getTitle(), borrowVO.getBorwhether());
			}
	}
		

	public BorrowVO inputborInfo() {
		BorrowVO borrowVO = new BorrowVO();
		System.out.print("제목 > ");
		borrowVO.setTitle(sc.nextLine());
		return borrowVO;
		
	}
	
	public void borUpdate() {
		BorrowVO borrowVO = inputBorData();
		borrowDAO.borUpdate(borrowVO);
		
	}

	public BorrowVO inputBorData() {
		BorrowVO borrowVO = new BorrowVO();
		
		System.out.println("대출일 > ");
		borrowVO.setBordate(sc.nextLine());
		System.out.println("전화번호 > ");
		borrowVO.setTel(sc.nextLine());
		System.out.println("도서번호 > ");
		borrowVO.setBno(Integer.parseInt(sc.nextLine()));
		return borrowVO;
	}
	
	public void loanUpdate() {
		LoanVO loanVO = inputLoanData();
		loanDAO.loanUpdate(loanVO);
	}


	private LoanVO inputLoanData() {
		LoanVO loanVO = new LoanVO();
		
		System.out.println("반납일 > ");
		loanVO.setLoandate(sc.nextLine());
		System.out.println("도서번호 > ");
		loanVO.setTel(sc.nextLine());
		
		
		return loanVO;
	}
	public void loanSelect() {
		
		List<LoanVO> list = loanDAO.loanSelect();

		if (list.isEmpty()) {
			System.out.print("대여중인 도서가 없습니다.");
			return;

		}
		for (LoanVO loanVO : list) {
			System.out.printf("%d. %s, %s, %s, %s , %s, %s \n", loanVO.getBno(), loanVO.getISBN(), loanVO.getCate(),
					loanVO.getTitle(), loanVO.getAuthor(), loanVO.getBordate(), loanVO.getTel());
		}
	}



	public void exit() {
		System.out.println("프로그램 종료");
	}
}
