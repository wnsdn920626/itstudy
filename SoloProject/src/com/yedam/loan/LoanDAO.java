package com.yedam.loan;

import java.util.List;

public interface LoanDAO {

	//반납
	
	public void loanUpdate(LoanVO loanVO);
	
	//미반납도서조회
	
	List<LoanVO> loanSelect();

	
	
	
	
}
