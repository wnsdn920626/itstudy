package com.yedam.loan;

import java.util.ArrayList;
import java.util.List;

import com.yedam.common.DAO;

public class LoanDAOImpl extends DAO implements LoanDAO {
	
	public static LoanDAO instance = null;
	public static LoanDAO getInstance() {
		if(instance == null)
			instance = new LoanDAOImpl();
		return instance;
	}
	
	
	//도서반납 -> 반납일자, 전화번호를 입력하면 대출가능여부가 0으로 바뀌고 전화번호칸이 null이 됨
	@Override
	public void loanUpdate(LoanVO loanVO) {
		
		try {
			connect();
			String sql = "UPDATE bookInfo SET loandate = ? , borwhether = 0, tel = null where tel = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, loanVO.getLoandate());
			pstmt.setString(2, loanVO.getTel());
			
			int result = pstmt.executeUpdate();
			if(result > 0) {
				System.out.println("정상적으로 반납되었습니다.");
			}else {
				System.out.println("반납되지않았습니다.");
			}
	}catch(Exception e) {
		e.printStackTrace();
	}finally {
		disconnect();
	}
	}
	
	//미반납도서 조회 -> 대출가능여부가 1(대출불가)인 행에 책의번호 ISBN 카테고리 타이틀 저자 대출일 전화번호 출력 
	@Override
	public List<LoanVO> loanSelect() {
		
		List<LoanVO> list = new ArrayList<>();
		try {
			connect();
			
			stmt = conn.createStatement();
			
			String sql = " SELECT * FROM bookInfo WHERE borwhether = 1";
			rs = stmt.executeQuery(sql);

			
			while(rs.next()) {
				LoanVO loanVO = new LoanVO();
				
				loanVO.setBno(rs.getInt("bno"));
				loanVO.setISBN(rs.getString("ISBN"));
				loanVO.setCate(rs.getString("cate"));
				loanVO.setTitle(rs.getString("title"));
				loanVO.setAuthor(rs.getString("author"));
				loanVO.setBordate(rs.getString("bordate"));
				loanVO.setTel(rs.getString("tel"));
				
				list.add(loanVO);
			}
		}catch(Exception e) {
			
		}finally {
			disconnect();
		}
		return list;
	}

}
