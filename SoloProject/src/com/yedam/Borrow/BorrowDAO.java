package com.yedam.Borrow;

import java.util.List;

import com.yedam.Book.BookVO;

public interface BorrowDAO {

	//도서대출(갱신)
	public void borUpdate(BorrowVO borrowVO);
	
	//대출가능조회
	List<BorrowVO> borSelect(BorrowVO borrowVO);
	
	
}
	
