package com.yedam.Borrow;

import java.util.ArrayList;
import java.util.List;

import com.yedam.Book.BookDAO;
import com.yedam.Book.BookDAOImpl;
import com.yedam.common.DAO;

public class BorrowDAOImpl extends DAO implements BorrowDAO{
	
	public static BorrowDAO instance = null;
	public static BorrowDAO getInstance() {
		if(instance == null)
			instance = new BorrowDAOImpl();
		return instance;
	}
	
	//대출여부 조회 -> 책의제목 입력시 1(대출불가능)을 제외한 결과값 출력
	@Override
	public List<BorrowVO> borSelect(BorrowVO borrowVO) {
		BorrowVO borrVO = null;
		List<BorrowVO> list = new ArrayList<>();
		
		try {
			 connect();
			 
			 String sql = " select * FROM bookInfo WHERE (title LIKE '%'||?||'%' AND borwhether != '1')";
			
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, borrowVO.getTitle());
			
			rs = pstmt.executeQuery();
			 
			
			
			while(rs.next()) {
				borrVO = new BorrowVO();
				
				borrVO.setTitle(rs.getString("title"));
				borrVO.setBorwhether(rs.getString("borwhether"));
				
				list.add(borrVO);
			}
					 
					 
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		return list;
	}
	
	
	
	
	@Override //도서대출 -> 대출일, 전화번호, 책번호를 입력하면 그 행의 대출여부가 0->1로 변경되고 반납일이 null로 초기화됨
	public void borUpdate(BorrowVO borrowVO) {
		try {
			connect();
			String sql = "UPDATE bookInfo SET bordate = ? , borwhether = 1 , loandate = null, tel = ? where bno = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, borrowVO.getBordate());
			pstmt.setString(2, borrowVO.getTel());
			pstmt.setInt(3, borrowVO.getBno());
			
			int result = pstmt.executeUpdate();
			if(result > 0 ) {
				System.out.println("정상적으로 대출되었습니다.");
			}else {
				System.out.println("대출이 되지않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		
	}




	

	

	
	
	
	
	
}
