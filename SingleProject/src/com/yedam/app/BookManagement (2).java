package com.yedam.app;

import java.util.List;
import java.util.Scanner;

import com.yedam.book.BookDAO;
import com.yedam.book.BookDAOImpl;
import com.yedam.book.BookVO;



public class BookManagement {

	Scanner sc = new Scanner(System.in);
	BookDAO bookDAO = BookDAOImpl.getInstance();
	
	public BookManagement() {
		while(true) {
			//메뉴출력
			menuPrint();
			
			//메뉴선택
			int menuNo = menuSelect();
			
			//메뉴 기능들
			
			if(menuNo == 1) {
				//도서등록
				insertBook();
			}else if(menuNo == 2){
				//도서수정
				updateBook();
			}else if(menuNo == 3){
				//도서삭제
				deleteBook();
			}else if(menuNo == 4){
				//전체조회
				selectAll();
			}else if(menuNo == 5){
				//개별조회
				selectOne();
			}else if(menuNo == 6){
				//도서대출
			}else if(menuNo == 7){
				//미반납도서조회
			}else if(menuNo == 0){
				//종료
				end();
				break;
			}else {
				//기타사항
				printErrorMessage();
			}
		}
	}
			
	private void printErrorMessage() {
		System.out.println("==============================");
		System.out.println(" 메뉴를 잘못 입력하였습니다.");
		System.out.println(" 메뉴를 다시 한번 확인해주세요.");
		System.out.println("==============================");
	}
	
	private void end() {
		System.out.println("=====================");
		System.out.println("프로그램을 종료합니다");
		System.out.println("=====================");
	}
	
	private void menuPrint() {
		System.out.println("===========================================================================================");
		System.out.println(" 1.도서등록 2.도서수정 3.도서삭제 4.전체조회 5.개별조회 6.도서대출 7.미납도서조회 0.프로그램종료");
		System.out.println("===========================================================================================");
	}
	
	private int menuSelect() {
		int menuNo = 0;
		try {
			menuNo = Integer.parseInt(sc.nextLine());
		}catch(NumberFormatException e) {
			System.out.println("메뉴는 숫자로 구성되어있습니다.");
		}
		 return menuNo;
	}
	
	private void selectAll() {
		List<BookVO> list = bookDAO.selectAll();
		if(list.isEmpty()) {
			System.out.println("정보가 존재하지 않습니다.");
			return;
		}
		for(BookVO bookVO : list) {
			System.out.printf("%s , %s, %s, %s, %d \n",bookVO.bookISBN, bookVO.bookTitle, bookVO.bookAuthor, bookVO.bookContent, bookVO.bookStock);
		}
	}
	
	private void selectOne() {
		BookVO findBook = inputBookInfo();
		BookVO bookVO = bookDAO.selectOne(findBook);
		if(bookVO == null) {
			System.out.printf("%s 도서는 존재하지 않습니다. \n", findBook.bookISBN);
		}else {
			System.out.println("검색결과 > ");
			System.out.println(bookVO);
		}
	}
	
	private void insertBook() {
		BookVO bookVO = inputBookAll();
		bookDAO.insert(bookVO);
	}
	
	private void updateBook() {
		BookVO bookVO = inputBookInfo();
		bookDAO.update(bookVO);
	}
	
	private void deleteBook() {
		String bookISBN = inputBookISBN();
		bookDAO.delete(bookISBN);
	}
	
	private BookVO inputBookAll() {
		BookVO bookVO = new BookVO();
		System.out.println("ISBN > ");
		bookVO.setBookISBN(sc.nextLine());
		System.out.println("도서제목 > ");
		bookVO.setBookTitle(sc.nextLine());
		System.out.println("저자 > ");
		bookVO.setBookAuthor(sc.nextLine());
		System.out.println("내용 > ");
		bookVO.setBookContent(sc.nextLine());
		System.out.println(" 재고 > ");
		bookVO.setBookStock(Integer.parseInt(sc.nextLine()));
		
		return bookVO;
	}
	
	private 
	
	
	
	
	
	
	
	
	
}
