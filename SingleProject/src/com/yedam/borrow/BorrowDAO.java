package com.yedam.borrow;

import java.util.List;

public interface BorrowDAO {

	
	
	//도서대출
	
	void insert(BorrowVO borrowVO);
	
	//도서반납
	
	void update(BorrowVO borrowVO);
	
	//미반납도서 조회
	
	List<BorrowVO> selectAll();
	
}
