package com.yedam.borrow;

public class BorrowVO {

	public String borDate;
	public String borTel;
	public String borInfo;
	public String deadlineDate;
	public String borWhether;
	public String returnDate;
	
	public String getBorDate() {
		return borDate;
	}
	public void setBorDate(String borDate) {
		this.borDate = borDate;
	}
	public String getBorTel() {
		return borTel;
	}
	public void setBorTel(String borTel) {
		this.borTel = borTel;
	}
	public String getBorInfo() {
		return borInfo;
	}
	public void setBorInfo(String borInfo) {
		this.borInfo = borInfo;
	}
	public String getDeadlineDate() {
		return deadlineDate;
	}
	public void setDeadlineDate(String deadlineDate) {
		this.deadlineDate = deadlineDate;
	}
	public String getBorWhether() {
		return borWhether;
	}
	public void setBorWhether(String borWhether) {
		this.borWhether = borWhether;
	}
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	
	
	
}
