package com.yedam.borrow;

import java.util.ArrayList;
import java.util.List;

import com.yedam.common.DAO;

public class BorrowDAOImpl extends DAO implements BorrowDAO{

	@Override
	public void insert(BorrowVO borrowVO) {
		try {
			connect();
			String sql = "INSERT INTO Borrow VALUES (?, ?, ?, ?, ?, ?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, borrowVO.borDate);
			pstmt.setString(2, borrowVO.borTel);
			pstmt.setString(3, borrowVO.borInfo);
			pstmt.setString(4, borrowVO.deadlineDate);
			pstmt.setString(5, borrowVO.borWhether);
			pstmt.setString(6, borrowVO.returnDate);
			
			int result = pstmt.executeUpdate();
			
			if(result > 0) {
				System.out.println("정상적으로 대출되었습니다.");
			}else {
				System.out.println("도서대출이 되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		
	}

	@Override
	public void update(BorrowVO borrowVO) {
		try {
			connect();
			String sql = "UPDATE Borrow SET "
		}
		
	}

	@Override
	public List<BorrowVO> selectAll() {
		List<BorrowVO> list = new ArrayList<>();
		try {
			connect();
			
			stmt = conn.createStatement();
			String sql = "SELECT * FROM Borrow";
			rs = stmt.executeQuery(sql);
//			int count = 0;
			while(rs.next()) {
				BorrowVO borrowVO = new BorrowVO();
				borrowVO.setBorDate(rs.getString("borDate"));
				borrowVO.setBorTel(rs.getString("borTel"));
				borrowVO.setBorInfo(rs.getString("borInfo"));
				list.add(borrowVO);
				
				if (borrowVO.returnDate == null);
			}
		}catch(Exception e) {
			
		}finally {
			disconnect();
		}
		return list;
	}

}
