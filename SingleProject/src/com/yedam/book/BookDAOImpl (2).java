package com.yedam.book;

import java.util.ArrayList;
import java.util.List;

import com.yedam.common.DAO;

public class BookDAOImpl extends DAO implements BookDAO {

	@Override
	public List<BookVO> selectAll() {
		List<BookVO> list = new ArrayList<>();
		try {
			connect();
			
			stmt = conn.createStatement();
			String sql = "SELECT * FROM BookInfo";
			rs = stmt.executeQuery(sql);
			int count = 0;
			while(rs.next()) {
				BookVO bookVO = new BookVO();
				bookVO.setBookISBN(rs.getString("bookISBN"));
				bookVO.setBookTitle(rs.getString("bookTitle"));
				bookVO.setBookAuthor(rs.getString("bookAuthor"));
				bookVO.setBookContent(rs.getString("bookContent"));
				bookVO.setBookStock(rs.getInt("bookStock"));
				list.add(bookVO);
				
				if (++count >= 26) break;
				}
			}catch(Exception e) {
				
			}finally {
				disconnect();
			
			}
			return list;
	}

	@Override
	public BookVO selectOne(BookVO bookVO) {
		BookVO findVO = null;
		try {
			connect();
			stmt = conn.createStatement();
			
			String sql = "SELECT * FROM BooKInfo where bookISBN, bookTitle, bookAuthor, bookContent " + bookVO.getBookISBN();
		    rs = stmt.executeQuery(sql);
			
			if(rs.next()) {
				findVO = new BookVO();
				findVO.setBookISBN(rs.getString("bookISBN"));
				findVO.setBookTitle(rs.getString("bookTitle"));
				findVO.setBookAuthor(rs.getString("bookAuthor"));
				findVO.setBookContent(rs.getString("bookContent"));
				findVO.setBookStock(rs.getInt("bookStock"));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				disconnect();
			}
		
		return findVO;
	}

	@Override
	public void insert(BookVO bookVO) {
		try {
			connect();
			String sql = "INSERT INTO BookInfo VALUES (?, ?, ?, ?, ?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, bookVO.bookISBN);
			pstmt.setString(2, bookVO.bookTitle);
			pstmt.setString(3, bookVO.bookAuthor);
			pstmt.setString(4, bookVO.bookContent);
			pstmt.setInt(5, bookVO.bookStock);
			
			int result = pstmt.executeUpdate();
			
			if(result > 0) {
				System.out.println("정상적으로 도서등록이 되었습니다.");
			}else {
				System.out.println("도서등록이 되지않았습니다.");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
	}


	@Override
	public void update(BookVO bookVO) {
		try {
			connect();
			String sql = "UPDATE BookInfo SET bookTitle, bookAuthor, bookContent = ? WHERE bookISBN = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, bookVO.getBookTitle());
			pstmt.setString(2, bookVO.bookAuthor);
			pstmt.setString(3, bookVO.bookContent);
			pstmt.setString(4, bookVO.bookISBN);
			
			int result = pstmt.executeUpdate();
			
			if(result > 0) {
				System.out.println("정상적으로 수정되었습니다.");
			}else {
				System.out.println("수정이 이루어지지않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}

	}

	@Override
	public void delete(String bookISBN) {
		try {
			connect();
			stmt = conn.createStatement();
			String sql = "DELETE FROM bookInfo WHERE bookISBN = " + bookISBN;
			int result = stmt.executeUpdate(sql);
		
			if(result > 0) {
			System.out.println("정상적으로 삭제되었습니다.");
			}else {
			System.out.println("삭제되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}

	}

	public static BookDAO getInstance() {
		// TODO Auto-generated method stub
		return null;
	}

}
