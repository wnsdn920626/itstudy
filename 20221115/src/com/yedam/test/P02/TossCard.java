package com.yedam.test.P02;

public class TossCard extends Card{
	
	String company = "Toss";
	String cardStaff;
	
	public TossCard(String cardNo, int validDate, int cvc, String company, String cardStaff) {
		this.cardNo = cardNo;
		this.validDate = validDate;
		this.cvc = cvc;
//		super(cardNo, validDate, cvc);
		this.company = company;
		this.cardStaff = cardStaff;
	}
	@Override
	public void showCardInfo() {
		System.out.println("카드 정보 - cardNo" + " , " + cardNo);
		System.out.println("담당 직원 - " + cardStaff + " , " + company);
	}
	
	
}
