package com.yedam.test.P02;

public class Card {
	
	//필드
	public String cardNo;
	public int validDate;
	public int cvc;
	
	//생성자
	
	
	public Card(String cardNo, int validDate, int cvc) {
		this.cardNo = cardNo;
		this.validDate = validDate;
		this.cvc = cvc;
	}
	
	
	//메소드 
	public void showCardInfo() {
		System.out.println("카드정보 ( Card NO : " + cardNo + " , 유효기간 : " + validDate + " , CVC : " + cvc);

	}
	
	
	
}
