package com.yedam.test.P02;

public class DGBCard extends Card{

	String company = "대구은행";
	String cardStaff;
	
	public DGBCard(String cardNo, int validDate, int cvc, String company, String cardStaff) {
		this.cardNo = cardNo;
		this.validDate = validDate;
		this.cvc = cvc;
//		super(cardNo, validDate, cvc);
		this.company = company;
		this.cardStaff = cardStaff;
	}
	@Override
	public void showCardInfo() {
		System.out.println("카드 정보 (cardNo : " + cardNo+ ", 유효기간 : " + validDate + ", CVC : " + cvc);
		System.out.println("담당 직원 - " + cardStaff + " , " + company);
	}
	
	
}
