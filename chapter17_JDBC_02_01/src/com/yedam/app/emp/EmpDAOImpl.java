package com.yedam.app.emp;

import java.util.ArrayList;
import java.util.List;

import com.yedam.app.common.DAO;

public class EmpDAOImpl extends DAO implements EmpDAO{
	
	
	private static EmpDAO instance = null;
	public static EmpDAO getInstance() {
		if(instance == null) 
			instance = new EmpDAOImpl();
		return instance;
	}
	@Override
	public List<EmpVO> selectAll() {
		List<EmpVO> list = new ArrayList<>();
		try {
			connect();
			
			stmt = conn.createStatement();
			String sql = "SELECT * FROM dept_manager";
			rs = stmt.executeQuery(sql);
			int count = 0;
			while(rs.next()) {
				EmpVO empVO = new EmpVO();
				empVO.setDeptNo(rs.getString("dept_no"));
				empVO.setEmpNo(rs.getInt("emp_no"));
				empVO.setFromDate(rs.getString("from_date"));
				empVO.setToDate(rs.getString("to_date"));
				list.add(empVO);
				
				if(++count >= 24) break;
			}	
		}catch(Exception e) {
			
		}finally {
			disconnect();
		}
		return list;
		
	}
	@Override
	public EmpVO selectOne(EmpVO empVO) {
		EmpVO findVO = null;
		try {
			connect();
			stmt = conn.createStatement();
			
			String sql = "SELECT * FROM dept_manager WHERE emp_no = " + empVO.getEmpNo();
			rs = stmt.executeQuery(sql);
			
			if(rs.next()) {
				findVO = new EmpVO();
				findVO.setDeptNo(rs.getString("dept_no"));
				findVO.setEmpNo(rs.getInt("emp_no"));
				findVO.setFromDate(rs.getString("from_date"));
				findVO.setToDate(rs.getString("to_date"));
			
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		
		}
		return findVO;
	}
	@Override
	public void insert(EmpVO empVO) {
		try {
			connect();
			String sql = "INSERT INTO dept_manager VALUES (?, ?, ?, ?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, empVO.getDeptNo());
			pstmt.setInt(2, empVO.getEmpNo());
			pstmt.setString(3, empVO.getFromDate());
			pstmt.setString(4, empVO.getToDate());
			
			int result = pstmt.executeUpdate();
			
			if(result >0) {
				System.out.println("정상적으로 등록되었습니다.");
			}else {
				System.out.println("정상적으로 등록되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally{
			disconnect();
		}
		
	}
	@Override
	public void update(EmpVO empVO) {
		try {
			connect();
			String sql = "UPDATE dept_manager SET dept_no = ? WHERE emp_no = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, empVO.getDeptNo());
			pstmt.setInt(2, empVO.getEmpNo());
			
			int result = pstmt.executeUpdate();
			
			if(result > 0) {
				System.out.println("정상적으로 수정되었습니다.");
			}else {
				System.out.println("정상적으로 수정되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		
		}
		
		
	}
	@Override
	public void delete(int empNo) {
		try {
			connect();
			stmt = conn.createStatement();
			String sql = "DELETE FROM dept_manager WHERE emp_no = " + empNo;
			int result = stmt.executeUpdate(sql);
			
			if(result > 0) {
				System.out.println("정상적으로 삭제되었습니다.");
			}else {
				System.out.println("정상적으로 삭제되지 않았습니다.");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		
	}
	
}
