package com.yedam.java.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLExample {

	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try { //JDBC는 무조건 6단계 반복
			// 1. JDBC Driver 로딩하기
			Class.forName("org.sqlite.JDBC");
			
			// 2. DBMS 서버와 접속하기
			String url ="jdbc:sqlite:/D:/dev/database/TestDataBase.db";
			conn = DriverManager.getConnection(url);
			/********************* INSERT ****************************/
			
			// 3. Statement or PreparedStatement 객체 생성하기
			String insert = "INSERT INTO students (student_id, student_name) "
							+ "VALUES (?,?)";
			//값을 넣고자 하는 곳에 ?를 넣음
			ps = conn.prepareStatement(insert);
			ps.setInt(1, 110); // 첫번째 물음표에 110을 넣어주세요.
			ps.setString(2, "윤달하" ); //두번째 물음표에 "윤달하"를 넣어주세요. 숫자일때는 따옴표 붙지않음
			
			// 4. SQL 실행하기
			
			int result = ps.executeUpdate();
			
			// 5. 결과 출력하기
			
			System.out.println("insert 결과 : " + result);
			
			/********************* SELECT ****************************/
			// 3. Statement or PreparedStatement 객체 생성하기
			
			stmt = conn.createStatement();
			
			// 4. SQL 실행하기
						
			String select = "SELECT student_id, student_name, student_dept FROM Students";
			rs = stmt.executeQuery(select);
			
			// 5. 결과 출력하기
			
			while(rs.next()) { //resultSet의 길이를 모르기때문에 
				int id = rs.getInt("student_id");
				String name = rs.getString("student_name");
				String dept = rs.getString(("student_dept"));
				
				System.out.printf("학번 : %d, 이름 : %s, 학년 : %s \n ",id, name, dept);
				
			}
			
			/********************* UPDATE ****************************/
			// 3. Statement or PreparedStatement 객체 생성하기
			
			String update = "UPDATE students SET student_dept = ? WHERE student_id = ?";
			ps = conn.prepareStatement(update);
			ps.setInt(2, 110);
			ps.setString(1, "3학년");
			
			// 4. SQL 실행하기
			
			result = ps.executeUpdate();
						
			// 5. 결과 출력하기
			
			System.out.println("update 결과 : " + result);
			
			/********************* DELETE ****************************/
			// 3. Statement or PreparedStatement 객체 생성하기
			
			stmt = conn.createStatement();
			
			// 4. SQL 실행하기
			
			String delete = "DELETE FROM students WHERE student_id = " + 110; //WHERE 없으면 다 삭제
			//DELETE FROM students WHERE student_id = 110
			result = stmt.executeUpdate(delete);
									
			// 5. 결과 출력하기
			System.out.println("delete 결과 : " + result);
			
		}catch(ClassNotFoundException e) {  // e = exception
			System.out.println("JDBC Driver Loading Fail");
		}catch(SQLException e){
			System.out.println("SQL관련 예외 : " + e.getMessage());
			e.printStackTrace();
		}catch(Exception e) { // <- 모든 예외의 부모격 기타 예외
			e.printStackTrace();
		}
		finally {
			
			try {
			// 6. 자원해제하기
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
			if(ps != null) ps.close();
			if(conn != null) conn.close();
			}catch(SQLException e) {
				System.out.println("정상적으로 자원이 해제되지 않았습니다.");
			}
		}
			
		
		
		
	}
}
