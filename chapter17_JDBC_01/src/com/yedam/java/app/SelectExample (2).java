package com.yedam.java.app;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class SelectExample {
	
	public static void main(String[] args) {
		try {
		// 1. JDBC Driver 로딩하기
		// 클래스를 직접 메모리상에 끌어올림
		Class.forName("org.sqlite.JDBC"); //Class.forName
		
		// 2. DB 서버 접속
		// 접속하는 경로가 정해져있음
		String url = "jdbc:sqlite:/D:/dev/database/TestDataBase.db";
		Connection conn = DriverManager.getConnection(url);
		
		// 3. Statement or PreparedStatement 객체 생성하기
		Statement stmt = conn.createStatement();          //new 안씀, 뭔가를 들고갈 준비
		
		// 4. SQL 실행
		ResultSet rs = stmt.executeQuery("SELECT student_id, student_name,student_dept From students");
		// ResultSet next() 에 while문 사용
		
		// 5. 결과값 출력하기
		while(rs.next()) {
			int sId = rs.getInt("student_id");
			String sName = rs.getString("student_name");
			String sDept = rs.getString("student_dept");
			System.out.println("학번 : " + sId);
			System.out.println("이름 : " + sName);
			System.out.println("학년 : " + sDept);
		}	
		}catch(ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
		// 6. 자원해제하기
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
			if(conn != null) conn.close();
		}
		// 1~5까지가 예외가 발생할수있는 파트

	}

}
